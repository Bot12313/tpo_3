import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdvancedSearchPage {

    private final WebDriver _driver;

    public AdvancedSearchPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[1]/div/div[2]/div[1]/input")
    private WebElement searchKeyWords;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[1]/div/div[2]/div[3]/label")
    private WebElement searchInName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[1]/div/div[2]/div[4]/label")
    private WebElement searchInCompany;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[6]/div/div[2]/div[1]/div[1]/input[1]")
    private WebElement salary;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[6]/div/div[2]/div[2]/label")
    private WebElement showOnlySalary;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[8]/div/div[2]/div[5]/label")
    private WebElement jobType_Intership;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[9]/div/div[2]/div[1]/label")
    private WebElement timeType_Fulltime;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form/div[9]/div/div[2]/div[3]/label")
    private WebElement timeType_Flex;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[3]/div/div[1]/div/form")
    private WebElement searchForm;

    /* -- */

    private void scrollToBottom() {
        WebElement element = _driver.findElement(By.xpath("/html/body/div[6]/div/div[2]/div/div/div[2]/div/div[2]/div/p[2]"));
        Actions actions = new Actions(_driver);
        actions.moveToElement(element);
        actions.perform();
    }

    public void searchFor(String search_value) {
        searchKeyWords.clear();
        searchKeyWords.sendKeys(search_value);
    }

    public void searchIn(boolean name, boolean company) {
        if (name != searchInName.findElement(By.xpath("./input")).isSelected()) searchInName.click();
        if (company != searchInCompany.findElement(By.xpath("./input")).isSelected()) searchInCompany.click();
    }

    public void setSalary(String salary_value) {
        scrollToBottom();

        salary.clear();
        salary.sendKeys(salary_value);

        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.elementToBeClickable(showOnlySalary));

        if(!showOnlySalary.findElement(By.xpath("./input")).isSelected()) showOnlySalary.click();
    }

    public void resetSalary() {
        scrollToBottom();

        salary.clear();

        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.elementToBeClickable(showOnlySalary));

        if(showOnlySalary.findElement(By.xpath("./input")).isSelected()) showOnlySalary.click();
    }

    public void searchForType(boolean interships) {
        scrollToBottom();
        if(interships != jobType_Intership.findElement(By.xpath("./input")).isSelected()) jobType_Intership.click();
    }

    public void searchForTime(boolean fulltime, boolean flex) {
        scrollToBottom();
        if(fulltime != timeType_Fulltime.findElement(By.xpath("./input")).isSelected()) timeType_Fulltime.click();
        if(flex != timeType_Flex.findElement(By.xpath("./input")).isSelected()) timeType_Flex.click();
    }

    public void Get() {
        _driver.get("https://spb.hh.ru/search/vacancy/advanced");
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.urlContains("https://spb.hh.ru/search/vacancy/advanced"));
    }

    public void search() {
        searchForm.submit();
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.urlContains("https://spb.hh.ru/search/vacancy"));
    }
}
