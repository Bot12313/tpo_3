import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.*;

public class ResumeFormPage {
    private final WebDriver _driver;

    public ResumeFormPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    //region: elements

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[2]/div[2]/div/div[1]/div/div[2]/div[3]/div[2]/div[1]/div/span/input")
    private WebElement firstName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[2]/div[2]/div/div[1]/div/div[2]/div[5]/div[2]/div[1]/div/span/input")
    private WebElement lastName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[2]/div[2]/div/div[3]/div[2]/div/div/div/input")
    private WebElement cityName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[1]/input")
    private WebElement bdDay;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[2]/div/select")
    private WebElement bdMonth;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[3]/div[2]/div/div[1]/div[1]/div/div[2]/div/div/div[3]/input")
    private WebElement bdYear;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[3]/div[2]/div/div[1]/div[3]/div/div[2]/div[1]/label")
    private WebElement manGender;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[3]/div[2]/div/div[3]/div[1]/div[1]/label")
    private WebElement haveSkills;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[4]/div[2]/div/div[1]/div[1]/div[2]/div/span/input")
    private WebElement career;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[4]/div[2]/div/div[1]/div[3]/div[2]/div[1]/div[1]/input")
    private WebElement salary;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[4]/div[2]/div/div[3]/div/div[1]/button")
    private WebElement profArea;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[1]/div/input")
    private WebElement profArea_search;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/div/div[1]/div[2]/div/div/div/label")
    private WebElement profArea_firstAfterSearch;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[4]/span[2]/button")
    private WebElement profArea_save;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[1]/div[2]/div/div/div/div[2]/div/button")
    private WebElement addJobPlace;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form/div/div[1]/div[2]/div/div/div[1]/div/div[1]/div/select")
    private WebElement addJobPlace_startMonth;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form/div/div[1]/div[2]/div/div/div[1]/div/div[2]/input")
    private WebElement addJobPlace_startYear;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form/div/div[4]/div/div[2]/div[2]/div[1]/div/div/input")
    private WebElement addJobPlace_organization;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form/div/div[4]/div/div[5]/div[2]/div[1]/div/div/input")
    private WebElement addJobPlace_speciality;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form/div/div[5]/div[2]/div/div/textarea")
    private WebElement addJobPlace_needToDo;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[3]/div[2]/button")
    private WebElement addJobPlace_save;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[2]/div/div/div[2]/div/div/span/textarea")
    private WebElement about;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[3]/div/div[2]/div/div/div[1]/div/div/div[1]/button")
    private WebElement deleteFirstSkill;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[3]/div/div[2]/div/div/div/div[1]/input")
    private WebElement skillName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[3]/div/div[2]/div/div/div/div[2]/button")
    private WebElement skillAdd;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[3]/div[2]/div/span")
    private WebElement eduAdd;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[1]/div[2]/div/select")
    private WebElement eduLevel; // bachelor

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[3]/div[1]/div[1]/div[2]/div/div/input")
    private WebElement eduUniversity;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[3]/div[1]/div[2]/div[2]/div/div/input")
    private WebElement eduFaculty;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[3]/div[1]/div[3]/div[2]/div/div/input")
    private WebElement eduSpecialty;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[6]/div[2]/div/div/div[3]/div[1]/div[4]/div[2]/div/div/div/div[1]/input")
    private WebElement eduEndYear;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[7]/div[2]/div/div[3]/div[2]/div/div/span")
    private WebElement langAdd;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[7]/div[2]/div/div[3]/div[2]/div[1]/div[1]/div[1]/div/div/div[3]/div/select")
    private WebElement langLevel; // c1

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/form")
    private WebElement allForm; // c1

    //endregion: elements

    /* -- */

    public void Get() {
        _driver.get("https://spb.hh.ru/applicant/resumes/short?resume=");
    }

    public void setName(String name) {
        firstName.clear();
        firstName.sendKeys(name);
    }

    public void setLastName(String last_name) {
        lastName.clear();
        lastName.sendKeys(last_name);
    }

    public void setBirthday(Date birthday) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.setTime(birthday);
        bdDay.sendKeys(String.valueOf(cal.get(Calendar.DAY_OF_MONTH) + 1));
        String month = new Formatter(new Locale("ru")).format("%tb", cal).toString();
        bdMonth.sendKeys(month.substring(0, 2)); // search month and select
        bdYear.sendKeys(String.valueOf(cal.get(Calendar.YEAR)));
    }

    public void setGender(boolean gender) throws Exception {
        if (gender)
            manGender.click();
        else
            throw new Exception("No such gender");
    }

    public void click_haveSkills() {
        haveSkills.click();
    }

    public void setCareer(String career_name) {
        career.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE)); // custom clear
        career.sendKeys(career_name);
    }

    public void setSalary(String salary_value) {
        salary.clear();
        salary.sendKeys(salary_value);
    }

    public String getFactSalary() {
        return salary.getAttribute("value");
    }

    //region: profarea

    public void profareaClear() {
        try {
            WebElement clearAll = _driver.findElement(By.xpath("/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[4]/div[2]/div/div[3]/div/div[1]/div/div/div/div[1]/button[2]"));
            clearAll.click();
        } catch(Exception e) {
            System.out.println("No profareas");
        }
    }

    public void profareaOpen() {
        profArea.click();
    }

    public void profareaSearch(String profarea_name) {
        profArea_search.clear();
        profArea_search.sendKeys(profarea_name);
    }

    public void profareaChooseFirst() {
        profArea_firstAfterSearch.click();
    }

    public void profareaSave() {
        profArea_save.click();
    }

    //endregion: profarea

    //region: lastjob

    public void lastJobOpen() {
        addJobPlace.click();
    }

    public void lastJobSetDate(Date lastJobStart) {
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("Europe/Moscow"));
        cal.setTime(lastJobStart);
        String month = new Formatter(new Locale("ru")).format("%tb", cal).toString();
        addJobPlace_startMonth.sendKeys(month.substring(0, 2)); // search month and select
        addJobPlace_startYear.sendKeys(String.valueOf(cal.get(Calendar.YEAR)));
    }

    public void lastJobSetCompany(String company_name) {
        addJobPlace_organization.clear();
        addJobPlace_organization.sendKeys(company_name);
    }

    public void lastJobSetSpeciality(String speciality) {
        addJobPlace_speciality.clear();
        addJobPlace_speciality.sendKeys(speciality);
    }

    public void lastJobSetNeedToDo(String to_do) {
        addJobPlace_needToDo.clear();
        addJobPlace_needToDo.sendKeys(to_do);
    }

    public void lastJobSave() {
        addJobPlace_save.click();
    }

    //endregion: lastjob

    public void setAbout(String about_value) {
        about.clear();
        about.sendKeys(about_value);
    }

    //region: skills

    public void skillsClear() {
        try {
            //noinspection InfiniteLoopStatement
            while (true) {
                WebElement clearFirst = _driver.findElement(By.xpath("/html/body/div[6]/div/div[1]/div[4]/div/div/div/form/div[5]/div[3]/div/div[2]/div/div/div[1]/div/div/div[1]/button"));
                clearFirst.click();
            }
        } catch(Exception e) {
            System.out.println("No skills found");
        }
    }

    public void skillsInput(String skill_name) {
        skillName.clear();
        skillName.sendKeys(skill_name);
    }

    public void skillsAdd() {
        skillAdd.click();
    }

    //endregion: skills

    //region: education

    public void educationAdd() {
        eduAdd.click();
    }

    public void educationSetLevel(String level) {
        eduLevel.sendKeys(level);
    }

    public void educationSetUniversity(String university) {
        eduUniversity.clear();
        eduUniversity.sendKeys(university);
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[8]")));
        eduUniversity.sendKeys(Keys.ESCAPE); // autofill
    }

    public void educationSetFaculty(String faculty) {
        eduFaculty.clear();
        eduFaculty.sendKeys(faculty);
    }

    public void educationSetSpeciality(String speciality) {
        eduSpecialty.clear();
        eduSpecialty.sendKeys(speciality);
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[8]")));
        eduSpecialty.sendKeys(Keys.ESCAPE); // autofill
    }

    public void educationSetEndYear(String endyear) {
        eduEndYear.clear();
        eduEndYear.sendKeys(endyear);
    }

    //endregion: education

    //region: lang

    public void languageAdd() {
        langAdd.click();
    }

    public void languageSetLevel(String level) {
        langLevel.sendKeys(level);
    }

    //endregion: lang

    public boolean checkFormUnfinished() {
        return _driver.findElements(By.xpath("//*[text()='Обязательное поле']")).size() > 0;
    }

    public void Save() {
        allForm.submit();
    }

    public void WaitTillSuccessfulMessage() {
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div[2]/div[1]/div[1]/h2")));
    }
}
