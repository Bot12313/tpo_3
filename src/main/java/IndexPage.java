import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class IndexPage {
    private final WebDriver _driver;

    public IndexPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    @FindBy(xpath = "/html/body/div[4]/div[1]/div/div/div[1]/div[6]/a")
    private WebElement _btnCreateResume;

    @FindBy(xpath = "/html/body/div[4]/div[1]/div/div/div[1]/div[1]/a")
    private WebElement _btnGetResumes;

    @FindBy(xpath = "/html/body/div[5]/div[1]/div/div/div[1]/a")
    private WebElement _homeLink;

    /* -- */

    public void Get() {
        _driver.get("https://hh.ru");
    }

    public void Go() {
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.elementToBeClickable(_homeLink));
    }

    public void click_createResume() {
        _btnCreateResume.click();
    }

    public void click_checkResume() {
        _btnGetResumes.click();
    }
}
