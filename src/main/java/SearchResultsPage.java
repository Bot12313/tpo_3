import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SearchResultsPage {
    private final WebDriver _driver;

    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    //region: elements

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div[3]/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div/span/span/span/a")
    private WebElement firstResultHeader;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div[3]/div[2]/div[2]/div/div[1]/div/div[1]/div[3]/div[1]/div/div[1]/div[1]/a | /html/body/div[6]/div/div[1]/div[4]/div/div/div[3]/div[2]/div[2]/div/div[1]/div/div[1]/div[3]/div[1]/div/div[2]/div[1]/a")
    private WebElement firstResultCompany;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div[3]/div[2]/div[2]/div/div[1]/div/div[1]/div[2]/div[2]/span")
    private WebElement firstResultSalary;

    //endregion: elements

    /* -- */

    public void clickFirstOffer() {
        firstResultHeader.click();
        List<String> tabs = new ArrayList<>(_driver.getWindowHandles());
        _driver.switchTo().window(tabs.get(1)); //switches to new tab

        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.urlContains("https://spb.hh.ru/vacancy/"));
    }

    public String getFirstHeader() {
        return firstResultHeader.getText();
    }

    public String getFirstCompany() {
        return firstResultCompany.getText();
    }

    public String getFirstSalary() {
        return firstResultSalary.getText();
    }
}
