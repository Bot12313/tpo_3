import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ResumeListPage {

    private final WebDriver _driver;

    public ResumeListPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div[1]/div[3]/div[2]/div/h3/a")
    private WebElement firstResume;

    /* -- */

    public void clickFirstResume() {
        firstResume.click();
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.urlContains("https://spb.hh.ru/resume/"));
    }
}
