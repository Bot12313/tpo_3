import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

public class JobOfferPage {
    private final WebDriver _driver;

    public JobOfferPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    //region: elements

    @FindBy(css = "[data-qa=vacancy-view-employment-mode]")
    private WebElement employingMode;

    //endregion: elements

    /* -- */

    public String getEmployingMode() {
        return employingMode.getText();
    }

    public void close() {
        _driver.close();
        List<String> tabs = new ArrayList<>(_driver.getWindowHandles());
        _driver.switchTo().window(tabs.get(0)); //switches to previous
    }
}
