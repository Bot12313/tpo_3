import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Selenium {
    public enum Browser {
        CHROME, FIREFOX
    }

    public static WebDriver InitDriver(Browser browser) {
        WebDriver webDriver;

        System.setProperty(
                "webdriver.chrome.driver",
                "Y:\\Java\\TPO\\3\\drivers\\chromedriver.exe"
        );

        System.setProperty(
                "webdriver.gecko.driver",
                "Y:\\Java\\TPO\\3\\drivers\\geckodriver.exe"
        );

        if (browser == Browser.CHROME) {
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--user-data-dir=Y:\\Java\\TPO\\3\\ChromeProfile","--profile-directory=Profile 1");
            webDriver = new ChromeDriver(options);
        }
        else // if (browser == Browser.FIREFOX)
            webDriver = new FirefoxDriver();

        // Settings
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return webDriver;
    }
}
