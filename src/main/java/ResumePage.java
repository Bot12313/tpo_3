import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class ResumePage {
    private final WebDriver _driver;

    public ResumePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this._driver = driver;
    }

    /* -- */

    //region: elements

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div[1]/div/div/div[1]/div[3]/h2/span")
    private WebElement fullName;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div/div[4]/div[1]/div/div/div[1]/div[1]/div[1]/h1/span/span")
    private WebElement speciality;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div/div[4]/div[1]/div/div/div[4]/div[2]/div/div/div/div/div/div")
    private List<WebElement> skills;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div/div[4]/div[1]/div/div/div[5]/div[2]/div/div/div/span")
    private WebElement about;

    @FindBy(xpath = "/html/body/div[6]/div/div[1]/div[4]/div/div/div/div/div/div[2]/div/div[1]/div/div/div/div[2]/div/div[2]/div/div/div/button[3]")
    private WebElement delete;

    @FindBy(xpath = "/html/body/div[8]/div/div[1]/div[2]/div/form[2]/button")
    private WebElement deleteConfirmation;

    //endregion: elements

    /* -- */

    public String getFullName() {
        return fullName.getText();
    }

    public String getSpeciality() {
        return speciality.getText();
    }

    public String[] getSkills() {
        List<String> rawSkills = new ArrayList<>();
        for (WebElement skill_element : skills) {
            rawSkills.add(skill_element.findElement(By.xpath("./span/span")).getText());
        }
        return rawSkills.toArray(new String[0]);
    }

    public String getAbout() {
        return about.getText();
    }

    public void delete() {
        delete.click();
        WebDriverWait wdw = new WebDriverWait(_driver, 4);
        wdw.until(ExpectedConditions.elementToBeClickable(deleteConfirmation));
        deleteConfirmation.click();
    }
}
