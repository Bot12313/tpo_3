import org.junit.jupiter.api.*;
import org.openqa.selenium.WebDriver;

import java.text.SimpleDateFormat;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.*;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class Test {
    private WebDriver _driver;
    private IndexPage _indexPage;
    private ResumeFormPage _resumeFormPage;
    private ResumeListPage _resumeListPage;
    private ResumePage _resumePage;
    private AdvancedSearchPage _advancedSearchPage;
    private SearchResultsPage _searchResultsPage;
    private JobOfferPage _jobOfferPage;

    @BeforeAll
    void Init() {
        _driver = Selenium.InitDriver(Selenium.Browser.CHROME);
        _indexPage = new IndexPage(_driver);
        _resumeFormPage = new ResumeFormPage(_driver);
        _resumeListPage = new ResumeListPage(_driver);
        _resumePage = new ResumePage(_driver);
        _advancedSearchPage = new AdvancedSearchPage(_driver);
        _searchResultsPage = new SearchResultsPage(_driver);
        _jobOfferPage = new JobOfferPage(_driver);
    }

    @org.junit.jupiter.api.Test
    @Order(1)
    void CreateResume() throws Exception {
        _indexPage.Get();
        assertEquals(_driver.getCurrentUrl(), "https://spb.hh.ru/", "Перенаправление с главной страницы на spb.hh.ru");

        _indexPage.click_createResume();
        assertTrue(_driver.getCurrentUrl().contains("https://spb.hh.ru/applicant/resumes/short?resume="), "Переход на форму создания резюме с главной");

        _resumeFormPage.setName("Артем");
        _resumeFormPage.setLastName("Носов");
        _resumeFormPage.setBirthday(new SimpleDateFormat("dd.MM.yyyy").parse("12.02.2000"));
        _resumeFormPage.setGender(true);
        _resumeFormPage.Save();
        assertTrue(_resumeFormPage.checkFormUnfinished());

        _resumeFormPage.click_haveSkills();
        _resumeFormPage.setCareer("Frontend developer");
        _resumeFormPage.setSalary("2a1a1a1a1a");
        assertEquals("21 111", _resumeFormPage.getFactSalary());

        _resumeFormPage.profareaClear();
        _resumeFormPage.profareaOpen();
        _resumeFormPage.profareaSearch("Программирование");
        _resumeFormPage.profareaChooseFirst();
        _resumeFormPage.profareaSave();

        _resumeFormPage.lastJobOpen();
        _resumeFormPage.lastJobSetDate(new SimpleDateFormat("dd.MM.yyyy").parse("01.08.2021"));
        _resumeFormPage.lastJobSetCompany("Insurance");
        _resumeFormPage.lastJobSetSpeciality("Frontend developer");
        _resumeFormPage.lastJobSetNeedToDo("Разработка дизайна и верстка");
        _resumeFormPage.lastJobSave();

        _resumeFormPage.setAbout("Текст \"О себе\"");
        _resumeFormPage.skillsClear();
        _resumeFormPage.skillsInput("Обучаемость");
        _resumeFormPage.skillsAdd();
        _resumeFormPage.skillsInput("Test");
        _resumeFormPage.skillsAdd();

        _resumeFormPage.educationSetLevel("Бакал");
        _resumeFormPage.educationAdd();
        _resumeFormPage.educationSetUniversity("ИТМО");
        _resumeFormPage.educationSetFaculty("ВТ");
        _resumeFormPage.educationSetSpeciality("Программист");
        _resumeFormPage.educationSetEndYear("2022");

        _resumeFormPage.languageAdd();
        _resumeFormPage.languageSetLevel("C1");
        _resumeFormPage.Save();

        _resumeFormPage.WaitTillSuccessfulMessage();
        assertTrue(_driver.getCurrentUrl().contains("https://spb.hh.ru/applicant/resumes/suitable_vacancies?resume="), "Резюме создано и опубликовано для вакансий");
    }

    @org.junit.jupiter.api.Test
    @Order(2)
    void CheckResume() {
        _indexPage.Get();
        assertEquals(_driver.getCurrentUrl(), "https://spb.hh.ru/", "Перенаправление с главной страницы на spb.hh.ru");

        _indexPage.click_checkResume();
        assertTrue(_driver.getCurrentUrl().contains("https://spb.hh.ru/applicant/resumes"), "Переход на страницу с моими резюме");

        _resumeListPage.clickFirstResume();
        assertTrue(_driver.getCurrentUrl().contains("https://spb.hh.ru/resume/"), "Переход на страницу с конкретным резюме");

        assertEquals("Носов Артем", _resumePage.getFullName(), "Резюме: ФИО");
        assertEquals("Frontend developer", _resumePage.getSpeciality(), "Резюме: специальность");
        assertArrayEquals(new String[]{"Обучаемость", "Test"}, _resumePage.getSkills());
        assertEquals("Текст \"О себе\"", _resumePage.getAbout(), "Резюме: о себе");
        _resumePage.delete();

        _indexPage.Get();
        assertEquals(_driver.getCurrentUrl(), "https://spb.hh.ru/", "Перенаправление с главной страницы на spb.hh.ru");

        _indexPage.click_checkResume();
        assertTrue(_driver.getCurrentUrl().contains("https://spb.hh.ru/applicant/resumes/short?resume="), "Переход на форму создания резюме с главной - потому что все резюме удалены");
    }

    @org.junit.jupiter.api.Test
    void FindJob() {
        _advancedSearchPage.Get();
        _advancedSearchPage.searchFor("Frontend");
        _advancedSearchPage.searchIn(true, false);
        _advancedSearchPage.search();
        assertTrue(_searchResultsPage.getFirstHeader().contains("Frontend"), "Frontend в названии вакансии");

        _advancedSearchPage.Get();
        _advancedSearchPage.searchFor("JetBrains");
        _advancedSearchPage.searchIn(false, true);
        _advancedSearchPage.search();
        assertTrue(_searchResultsPage.getFirstCompany().contains("JetBrains"), "JetBrains в названии компании");

        _advancedSearchPage.Get();
        _advancedSearchPage.searchFor("");
        _advancedSearchPage.searchIn(false, false);
        _advancedSearchPage.setSalary("100000");
        _advancedSearchPage.search();
        String salary = _searchResultsPage.getFirstSalary();
        String regex = ".*?\\d{3,} \\d{3,} руб.";
        assertTrue(salary.matches(regex), "Зарплата от 100к");

        _advancedSearchPage.Get();
        _advancedSearchPage.resetSalary();
        _advancedSearchPage.searchForType(true);
        _advancedSearchPage.search();
        _searchResultsPage.clickFirstOffer();
        assertTrue(_jobOfferPage.getEmployingMode().toLowerCase(Locale.ROOT).contains("стажировка"), "Можно на стажировку");
        _jobOfferPage.close();

        _advancedSearchPage.Get();
        _advancedSearchPage.searchForType(false);
        _advancedSearchPage.searchForTime(true, false);
        _advancedSearchPage.search();
        _searchResultsPage.clickFirstOffer();
        assertTrue(_jobOfferPage.getEmployingMode().toLowerCase(Locale.ROOT).contains("полный день"), "Можно на полный день");
        _jobOfferPage.close();

        _advancedSearchPage.Get();
        _advancedSearchPage.searchForType(false);
        _advancedSearchPage.searchForTime(false, true);
        _advancedSearchPage.search();
        _searchResultsPage.clickFirstOffer();
        assertTrue(_jobOfferPage.getEmployingMode().toLowerCase(Locale.ROOT).contains("гибкий график"), "Можно на гибкий график");
        _jobOfferPage.close();
    }

    @AfterAll
    void Free() {

    }
}
